#include "gfx.h"
#include "WormsPool.h"
#include <unistd.h>
#include <iostream>
#include <iterator>
using namespace std;
int main()
{
    const int tam = 500;
    int nWorms, t;
    cout << "Ingrese el numero de gusanos: ";
    cin >> nWorms;
    WormsPool pool(nWorms, tam);
    gfx_open(tam, tam, "WORMS");
    gfx_color(0, 200, 100);
    while (1)
    {
        gfx_clear();
        vector<Worm> Worms = pool.getWorms();
        for (vector<Worm>::iterator i = Worms.begin(); i != Worms.end(); i++)
        {
            vector<Coordenada> Points = i->getPoints();
            for (vector<Coordenada>::iterator XY = Points.begin(); XY != Points.end(); XY++)
            {
                gfx_point(XY->getX(), XY->getY());
            }
        }
        gfx_flush();
        usleep(41666); //24 por segundo
        pool.moveWorms();
    }
    return 0;
}