#include "Coordenada.h"
#include <vector>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;
#ifndef WORM_H_
#define WORM_H_
class Worm
{
private:
    vector<Coordenada> Puntos;
    int poolSize;
    int movement;
    int direction;
    int initialX;
    int initialY;
    int radio = 0;
    int wormSize = 40;

public:
    Worm(int mov, int dir, int initX, int initY, int size);
    vector<Coordenada> getPoints();
    void move(int currentPixel);
};
#endif