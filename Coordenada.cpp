#include "Coordenada.h"

Coordenada::Coordenada(int xx, int yy) : x(xx), y(yy) {}

int Coordenada::getX()
{
    return x;
}

int Coordenada::getY()
{
    return y;
}