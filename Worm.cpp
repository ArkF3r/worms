#include "Worm.h"

Worm::Worm(int mov, int dir, int initX, int initY, int size) : movement(mov), direction(dir), initialX(initX), initialY(initY), poolSize(size)
{
    if (movement == 5)
    {
        radio = rand() % 27 + 15;
    }
    move(0);
}

vector<Coordenada> Worm::getPoints()
{
    return Puntos;
}

void Worm::move(int currentPixel)
{
    Puntos.clear();
    int X, Y, factor;
    switch (movement)
    {
    case 0:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point(x1 + 3 + 3 * sin(16 * 0.01745334 * (720.0 / tam) * factor), (tam + y1 + factor) % tam);
            int factor = direction * (currentPixel + i);
            X = initialX + 3 + 3 * sin(16 * 0.01745334 * (720.0 / poolSize) * factor);
            Y = (poolSize + initialY + factor) % poolSize;
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    case 1:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point((tam + x1 + factor) % tam, y1 + 3 + 3 * sin(16 * 0.01745334 * (720.0 / tam) * factor));
            factor = direction * (currentPixel + i);
            X = (poolSize + initialX + factor) % poolSize;
            Y = initialY + 3 + 3 * sin(16 * 0.01745334 * (720.0 / poolSize) * factor);
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    case 2:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point((tam + x3 + factor) % tam, y3);
            factor = direction * (currentPixel + i);
            X = (poolSize + initialX + factor) % poolSize;
            Y = initialY;
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    case 3:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point(x6, (tam + y6 + factor) % tam);
            factor = direction * (currentPixel + i);
            X = initialX;
            Y = (poolSize + initialY + factor) % poolSize;
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    case 4:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point((tam + x2 + factor) % (tam), (tam + y2 + factor) % (tam));
            factor = direction * (currentPixel + i);
            X = (poolSize + initialX + factor) % (poolSize);
            Y = (poolSize + initialY + factor) % (poolSize);
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    case 5:
        for (int i = 0; i < wormSize; i++)
        {
            // gfx_point(x3 + r1 * cos(0.01745334 * (720.0 / tam) * factor), y3 + r1 * sin(0.01745334 * (720.0 / tam) * factor));
            factor = direction * (currentPixel + i);
            X = static_cast<int>(initialX + radio * cos(0.01745334 * (720.0 / poolSize) * factor)) % poolSize;
            Y = static_cast<int>(initialY + radio * sin(0.01745334 * (720.0 / poolSize) * factor)) % poolSize;
            Puntos.push_back(Coordenada(X, Y));
        }
        break;
    default:
        break;
    }
}