#include "WormsPool.h"
#include <unistd.h>
#include <math.h>
#include <cstdlib>
#include <ctime>

WormsPool::WormsPool(int n, int tam) : qWorms(n), winTam(tam), currentPixel(0)
{
    srand(time(NULL));
    int iX, iY, dir, mov;
    for (int i = 0; i < qWorms; i++)
    {
        mov = rand() % 6;
        dir = pow(-1, rand() % 2);
        iX = rand() % winTam;
        iY = rand() % winTam;
        Worms.push_back(Worm(mov, dir, iX, iY, winTam));
    }
}

void WormsPool::moveWorms()
{
    if (currentPixel > winTam)
    {
        currentPixel = 0;
    }
    for (vector<Worm>::iterator i = Worms.begin(); i != Worms.end(); i++)
    {
        i->move(currentPixel);
    }
    currentPixel++;
}

vector<Worm> WormsPool::getWorms()
{
    return Worms;
}