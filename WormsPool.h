#include "Worm.h"
#include <unistd.h>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <vector>
#ifndef WORMSPOOL_H_
#define WORMSPOOL_H_
class WormsPool
{
private:
    int qWorms;
    int winTam;
    int currentPixel;
    vector<Worm> Worms;

public:
    WormsPool(int n, int tam);
    vector<Worm> getWorms();
    void moveWorms();
};
#endif