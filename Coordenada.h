#ifndef COORDENADA_H_
#define COORDENADA_H_

class Coordenada
{
private:
    int x;
    int y;

public:
    Coordenada(int = 0, int = 0);
    int getX();
    int getY();
};

#endif